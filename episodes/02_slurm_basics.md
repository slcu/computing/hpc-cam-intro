[^ lesson home](../README.md)  |  [< previous episode](./01_move_data.md)  |  [next episode >](./03_managing_software.md)

----

# Submit a job using SLURM

## Lesson objectives

- Understand the basic concept of how SLURM works
- Submit a simple job
- Understand SLURM environment variables

## Submitting a simple job

To submit a job to SLURM, you need to include your code in a *shell script*.
Options for running the job go at the top of the script using the `#SBATCH` keyword, followed by the SLURM option.
The script is then submitted to SLURM using the `sbatch` program.

Here is an example of a simple job:

```bash
#!/bin/bash

#SBATCH -A <FIXME>
#SBATCH -J simple_job
#SBATCH -D /home/<FIXME>/rds/hpc-work/projects/hpc_workshop
#SBATCH -o scripts/first_job.log
#SBATCH -p skylake             # or skylake-himem
#SBATCH -c 2                   # max 32 CPUs
#SBATCH --mem-per-cpu=1000MB   # max 5980MB or 12030MB for skilake-himem
#SBATCH -t 00:02:00            # HH:MM:SS with maximum 12:00:00 for SL3 or 36:00:00 for SL2

echo "This is output to stdout"

echo "This is re-directed to a file of choice" > first_job_output.txt

# SLURM defines some variables automatically, which can be useful when writing scripts
echo "This is job was assigned $SLURM_CPUS_PER_TASK CPUs."
```

Let's break this down:

1. The first line (`#!/bin/bash`) is called a [_shebang_](https://en.wikipedia.org/wiki/Shebang_(Unix)) and indicates which program should interpret this script. In this case, _bash_ is the interpreter of _shell_ scripts (there's other shell interpreters, but that's beyond what we need to worry about here). Basically just **always have this as the first line of your script**.
2. Lines starting with `#SBATCH` contain options for the _sbatch_ program. These options are:
    - `-A` is the billing account, needed if you're using the Cambridge University HPC. You may have more than one (e.g. if you have different [service levels (SL)](https://docs.hpc.cam.ac.uk/hpc/user-guide/policies.html#service-levels), such as the free "SL3" and paid "SL2"), so pick the right one - check them with `mybalance`.
    - `-J` defines a name for the job.
    - `-D` defines the *working directory* used for this job.
    - `-o` defines the file where the output that would normally be printed on the console is saved in. This is defined in relation to the working directory set above. (technically this will include both the _standard output_ and _standard error_)
    - `-c` is the number of CPUs you want to use for your job.
    - `-t` is the time you need for your job to run. This is not always possible to know in advance, so if you're unsure leave it blank. However, if you know your jobs are short, setting this option will make them start faster.
    - `-p` is the *partition*, needed when using the Cambridge University HPC. There are two of these `skylake` and `skylake-himem`, which allow a maximum of 5980MB and 12030MB or RAM per CPU, respectively.
    - `--mem-per-cpu=` defines how much RAM memory you want for your job. You can instead use the option `--mem` if you prefer to define the total memory you require for your job (but check the Billing details in the section below).
3. The other lines are the commands that we actually want to run.
   - Any output that would have been printed on the console (the first and third `echo` commands) will now be put into the file specified with the `#SBATCH -o` option above.
   - Any output that is redirected to a specific file (the second `echo`) will create that file as normal.
   - There are some shell _environment variables_ which SLURM creates that automatically store values that match the resources we request for our job. In the example above "$SLURM_CPUS_PER_TASK" will contain the value 2 because we requested 2 CPUs (`#SBATCH -c 2`)

**Tip - test your jobs faster:**

`#SBATCH --qos=intr` option can be used when _testing_ scripts. This will allocate a maximum of 1h to your job in the highest priority queue. Only one of these jobs is allowed to run at a time and after the 1h the job will be killed, so it should only be used for _testing_ scripts.


### Do it Yourself

- Create a directory called `scripts` in `~/rds/hpc-work/projects/hpc_workshop`
- Copy the code above and save it in a shell script named `first_job.sh`
  - use the text editor on your machine and save the script using the mounted drive (as covered in the [previous lesson](./01_move_data.md))
  - don't forget to modify the `-A` option with your billing account and adjust the `-o` path with your username
- Submit the script to SLURM using `sbatch`

```bash
# submitting the job
sbatch ~/rds/hpc-work/hpc_workshop/scripts/first_job.sh
```

### Default Resource Options

If you don't specify some of the options listed above, this is the default you will get:

- 10 minutes of running time (`-t 00:10:00`)
- _skylake_ partition (`-p skylake`)
- 1 CPU (`-c 1`)
- 5980MB RAM (`--mem=5980MB` or `--mem-per-cpu=5980MB`)


## Checking job status

After submitting a job, you can check its status using:

```bash
# show job status
squeue -u <user>

# show details about the job, such as the working directory and output directories
scontrol show job <jobid>
```

This gives you information about the job's status, `PD` means it's *pending* (waiting in the queue) and `R` means it's *running*.


## Cancel a job

To cancel a job do:

```bash
# replace <JOBID> with the job ID from squeue
scancel <JOBID>
```


## Checking resource usage

To check the statistics about a job:

```bash
sacct -o jobname,account,state,reqmem,maxrss,averss,elapsed -j <JOBID>
```

- `jobname` is the job's name
- `account` is the account used for the job
- `state` gives you the state of the job
- `reqmem` is the memory that you asked for (Mc is MB per core)
- `maxrss` is the maximum memory used during the job *per core*
- `averss` is the average memory used *per core*
- `elapsed` how much time it took to run your job

This can help you determine suitable resources (e.g. RAM, time) next time you run a similar job.

**Note:** this only works on the university HPC, not the SLCU one.

## Check account details

- `mybalance` to check how many hours of usage you have available on your account(s)
- `quota` to check how much disk space you have available in `/home` and `/rds`

**Note:** this only works on the university HPC, not the SLCU one.


### Do it Yourself

- Check how many resources your previous job used.
  - note: if you can't remember the JOBID running `sacct` with no other options will show you which jobs were run by you recently.


## Billing

The billing on the University HPC is done by CPU-hour. Here's some examples:

- You requested 3 CPUs (`-c 3`) and 10 hours (`-t 10:00:00`). Your job only took 2 hours to finish. You are charged 3*2 = 6 hours of compute time.
- You requested 1 CPU (`-c 1`) and 15000MB of total RAM (`--mem=15000MB`) on _skylake-himem_ (`-p skylake-himem`), and the job took 1 hour to run. Because this partition provides 12030MB per CPU, you will actually be charged for 2 CPUs, so 2*1 = 2 hours of compute time.

If you're using a SL3 account (free), your allowance is capped. Each PI receives 200,000 CPU hours per quarter.


## Long Jobs

As a standard you are limited to 12h for jobs on the University HPC. Long jobs (up to 7 days) can be run on special queues, for which you need to request access. See instructions on the [documentation page](https://docs.hpc.cam.ac.uk/hpc/user-guide/long.html).

If this is not enough, then you can run jobs on the SLCU HPC, which has no time limit.


## SLCU HPC

The usage on the SLCU HPC is similar, but since we do not have billing accounts you don't need to specify the `-A` option or the `-p` option.

Here is a skeleton script, equivalent to the one above:

```bash
#!/bin/bash

#SBATCH -J simple_job
#SBATCH -D /home/<FIXME>/projects/hpc_workshop
#SBATCH -o logs/first_job.log
#SBATCH -c 2            # max 48 CPUs
#SBATCH --mem=1000MB    # max 500GB
#SBATCH -t 00:02:00     # HH:MM:SS

echo "This is output to stdout"

echo "This is re-directed to a file of choice" > first_job_output.txt

# SLURM defines some variables automatically, which can be useful when writing scripts
echo "This is job was assigned $SLURM_CPUS_PER_TASK CPUs."
```

If you don't specify the resources you want, this is what you'll get by default:

- 1 CPU (`-c 1`)
- 20000MB RAM (`--mem=20000MB`)
- Infinite running time

Finally, you can use these other commands, as detailed above:

- `squeue` to list jobs in the queue
- `scancel <JOBID>` to cancel a job
- `scontrol show job <JOBID>` for more detailed information about your job

(note: `sacct`, `mybalance` and `quota` are not available commands on the SLCU HPC)


## Further resources

- [SLURM cheatsheet](https://slurm.schedmd.com/pdfs/summary.pdf)
- UIS documentation:
  - [Running jobs](https://docs.hpc.cam.ac.uk/hpc/user-guide/batch.html)
  - [Billing policies](https://docs.hpc.cam.ac.uk/hpc/user-guide/policies.html)

----

[^ lesson home](../README.md)  |  [< previous episode](./01_move_data.md)  |  [next episode >](./03_managing_software.md)
