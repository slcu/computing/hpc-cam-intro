[back to lesson home](../README.md)

----

# Setup instructions

There are four recommended pieces of software needed to work with the HPC:

- Unix terminal
- file transfer software
- filesystem software to interact with files stored on the HPC
- a text editor

## Mac

- Unix terminal: Mac OS already has a terminal available.
Press <kbd><kbd>&#8984;</kbd> + <kbd>space</kbd></kbd> to open _spotlight search_ and type "terminal".

- File transfer software:
  - command line: `rsync` or `scp` are available from the terminal.
  - GUI (optional): install [filezilla](https://filezilla-project.org/download.php?type=client).

- filesystem client:
  - download and install both FUSE and SSHFS from [this website](https://osxfuse.github.io/).
  (first install FUSE, then SSHFS)

- text editor:
  - Download and install [VSCode](https://code.visualstudio.com/).


## Linux (Ubuntu)

- Unix terminal: Ubuntu already has a terminal available.
Press <kbd><kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>T</kbd></kbd> to open it.

- File transfer software:
  - command line: `rsync` or `scp` are available from the terminal.
  - GUI (optional): install [filezilla](https://filezilla-project.org/download.php?type=client).

- filesystem client:
  - install SSHFS using the command `sudo apt-get install sshfs`.

- text editor:
  - Ubuntu comes with `gedit`, which is a decent text editor.
  - Alternatively you could try [VSCode](https://code.visualstudio.com/).

## Windows

- Unix terminal:
  <!-- - **Windows 10**: open "_Microsoft Store_" (from your windows menu) and search for "_Ubuntu_", select it and install the app. -->
  - Download and install the "_Installer edition_" of [MobaXterm - Home Edition](https://mobaxterm.mobatek.net/download-home-edition.html) (do not choose the "_Portable edition_"). Unzip the file and run the `.msi` file to install using default options.


- File transfer software:
  - command line: `rsync` or `scp` are available with _MobaXterm_.
  - GUI (optional): install [filezilla](https://filezilla-project.org/download.php?show_all=1) (choose the `win64-setup.exe` file).

- filesystem client:
  - Download and install [SFTP Drive Personal Edition](https://www.nsoftware.com/sftp/drive/download.aspx)
    - It will ask for your email for download and installation. If you want, use a [10 minute disposable email](https://10minutemail.com/10MinuteMail/index.html) to avoid potential spam.

<!--
  - Install sshfs-win following these instructions:
    - Go to the [WinFsp download page](https://github.com/billziss-gh/winfsp/releases/latest) and download the `.msi` file at the bottom of the page. After download run it and follow installation instructions (accept all default options).
    - Then go to the [sshfs-win download page](https://github.com/billziss-gh/sshfs-win/releases/latest) and download the `x64.msi` file. After download run it and follow installation instructions (accept all default options).
-->

- text editor:
  - Download and install [VSCode](https://code.visualstudio.com/).
  - After installation, open it and go to "_File > Preferences > Settings_", then select "_Text Editor > Files_" on the drop-down menu on the left and under the section named "_EOL_" choose "_\n_" (this will ensure that the files you edit on Windows are compatible with the Linux operating system on the HPC).

  ----

  [back to lesson home](../README.md)
